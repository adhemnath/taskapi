﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskMgr.Model
{
    public class Task
    {
        [Key]
        public int Task_ID { get; set; }
        public string TaskName { get; set; }
        public int Parent_ID { get; set; }
        public int? Priority { get; set; }        
        [Column(TypeName = "Date")]
        public DateTime Start_Date {get;set;}
        [Column(TypeName = "Date")]
        public DateTime End_Date { get; set; }
        public bool isTaskEnd { get; set; }
    }
}
