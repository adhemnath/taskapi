﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TaskMgr.Model;
using TaskMgr.Business;

namespace TaskMgr.Controllers
{
    public class TaskController:ApiController
    {        
        [HttpGet]
        [Route("GetTasksByTaskId")]
        public IHttpActionResult GetTasksByTaskId(int id)
        {
            using (TaskBusiness bus = new TaskBusiness())
            {
                Task selTeask = bus.GetTasksByTaskId(id);
                return Ok(selTeask);
            }
        }

        [HttpGet]
        [Route("GetAllTasks")]
        public IHttpActionResult GetAllTasks()
        {
            using (TaskBusiness bus = new TaskBusiness())
            {
                List<Task> alltasks =  bus.GetAllTasks();
                return Ok(alltasks);
            }
        }
      
        //[HttpGet]
        //[Route("DeleteTask")]
        //public IHttpActionResult DeleteTask(int id)
        //{

        //    using (TaskBusiness bus = new TaskBusiness())
        //    {
        //        bool isDeleted = bus.deleteTask(id);
        //        return Ok(isDeleted);
        //    }
        //}

        [HttpGet]
        [Route("EndTask")]
        public IHttpActionResult EndTask(int id)
        {

            using (TaskBusiness bus = new TaskBusiness())
            {
                bool isEnded = bus.endTask(id);
                return Ok(isEnded);
            }
        }

        [HttpPost]
        [Route("AddTask")]
        public IHttpActionResult AddTask([FromBody] Task tskNew)
        {
            using (TaskBusiness bus = new TaskBusiness())
            {
                bus.addTask(tskNew);
                return Ok(tskNew);
            }
        }

        [HttpPut]
        [Route("UpdateTask")]
        public IHttpActionResult UpdateTask([FromBody] Task tskUpdate)
        {
            using (TaskBusiness bus = new TaskBusiness())
            {
                bus.updateTask(tskUpdate);
                return Ok(tskUpdate);
            }
        }

    }
}