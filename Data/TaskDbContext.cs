﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using TaskMgr.Model;
using System.Collections;
using System.Data.Entity.Migrations;

namespace TaskMgr.Data
{
    public class TaskDbContext:DbContext
    {
        public TaskDbContext() : base("name=DBConnect")
        {
        }
        public DbSet<Task> Tasks { get; set; }

    }

    internal sealed class DatabaseInitializer : DropCreateDatabaseIfModelChanges<TaskDbContext>
    {
        protected override void Seed(TaskDbContext context)
        {
            base.Seed(context);
            //var Task1 = new List<Task>{new Task {TaskName="test", Parent_ID=0, Priority=10, Start_Date=Convert.ToDateTime("07/30/2019"), End_Date= Convert.ToDateTime("08/05/2019") } ,
            //                            new Task {TaskName="test1", Parent_ID=1, Priority=20, Start_Date=Convert.ToDateTime("07/30/2019"), End_Date= Convert.ToDateTime("08/05/2019") } };

            var Task1 = new Task { TaskName = "test", Parent_ID = 0, Priority = 10, Start_Date = Convert.ToDateTime("07/30/2019"), End_Date = Convert.ToDateTime("08/05/2019") };
                                        
            context.Tasks.Add(Task1);
            context.SaveChanges();

        }
    }
}
