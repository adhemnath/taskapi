namespace TaskMgr.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Task_ID = c.Int(nullable: false, identity: true),
                        TaskName = c.String(),
                        Parent_ID = c.Int(nullable: false),
                        Priority = c.Int(),
                        Start_Date = c.DateTime(nullable: false, storeType: "date"),
                        End_Date = c.DateTime(nullable: false, storeType: "date")                        
                })
                .PrimaryKey(t => t.Task_ID);            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tasks");
        }
    }
}
