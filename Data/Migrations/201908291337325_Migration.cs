namespace TaskMgr.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEndTaskColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tasks", "isTaskEnd", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tasks", "isTaskEnd");
        }
    }
}
