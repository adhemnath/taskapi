﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using TaskMgr.Controllers;
using TaskMgr.Model;

namespace TskMgrUnitTest
{
    [TestFixture]
    public class TaskMgrUnitTest
    {
        [Test]
        public void AddTasks_Test()
        {
            Task taskDetails = new Task();
            //taskDetails.Task_ID = 2000;
            taskDetails.TaskName = "TestingUnit";
            taskDetails.Parent_ID = 5;
            taskDetails.Priority = 4;
            taskDetails.Start_Date = System.DateTime.Now;
            taskDetails.End_Date = System.DateTime.Now.AddDays(1);

            TaskController tc = new TaskController();

            IHttpActionResult result = tc.AddTask(taskDetails);
            var actual = result as OkNegotiatedContentResult<Task>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }
        [Test]
        public void UpdateTasks_Test()
        {
            TaskController tc = new TaskController();
            Task taskDetails = new Task();
            taskDetails.TaskName = "TestingUnit14";
            taskDetails.Parent_ID = 5;
            taskDetails.Priority = 4;
            taskDetails.Start_Date = System.DateTime.Now;
            taskDetails.End_Date = System.DateTime.Now.AddDays(2);
            taskDetails.Task_ID = 3290;
            var result = tc.UpdateTask(taskDetails);
            var actual = result as OkNegotiatedContentResult<Task>;
            Assert.IsNotNull(actual);

        }
        [Test]
        public void GetAllTasks_Test()
        {
            TaskController controller = new TaskController();
            var result = controller.GetAllTasks();
            var actual = result as OkNegotiatedContentResult<List<Task>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }
        [Test]
        public void GetTasksByTaskId_Test()
        {
            TaskController controller = new TaskController();
            var result = controller.GetTasksByTaskId(3290);
            var actual = result as OkNegotiatedContentResult<Task>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }
        //[Test]
        //public void deleteTasks_Test()
        //{
        //    TaskController controller = new TaskController();
        //    var result = controller.DeleteTask(2000);
        //    var actual = result as OkNegotiatedContentResult<bool>;
        //    Assert.IsNotNull(actual);
        //    Assert.IsNotNull(actual.Content);
        //}
    }
}
