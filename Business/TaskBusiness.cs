﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskMgr.Data;
using TaskMgr.Model;

namespace TaskMgr.Business
{
    public class TaskBusiness : IDisposable
    {

        public void Dispose()
        {
            Console.WriteLine("Releasing all the resources......");
        }

        public List<Task> GetAllTasks()
        {
            using (TaskDbContext _db = new TaskDbContext())
            {
                return _db.Tasks.ToList();
            } 
           
        }

        public Task GetTasksByTaskId(int id)
        {
            using (TaskDbContext _db = new TaskDbContext())
            {
                //List<Task> allTasks = (from task1 in _db.Tasks
                //                       select task1).ToList();

                //Task task = allTasks.Where(a => a.Task_ID == id).FirstOrDefault();

                var result = _db.Tasks.FirstOrDefault(a => a.Task_ID == id);

                return result;
            }

        }

        public void updateTask(Task tskUpdate)
        {

            using (TaskDbContext _db = new TaskDbContext())
            {               
                var delteRecord = _db.Tasks.First(a => a.Task_ID == tskUpdate.Task_ID);
                if (delteRecord != null)
                {
                    delteRecord.Task_ID = tskUpdate.Task_ID;
                    delteRecord.TaskName = tskUpdate.TaskName;
                    delteRecord.Parent_ID = tskUpdate.Parent_ID;
                    delteRecord.Priority = tskUpdate.Priority;
                    delteRecord.Start_Date = tskUpdate.Start_Date;
                    delteRecord.End_Date =tskUpdate.End_Date;
                    _db.SaveChanges();
                }
                else
                {
                    throw new ApplicationException(string.Format("Task - {0}  Unable to update the Task", tskUpdate.Task_ID));
                }
            }            
        }

        public void addTask(Task tskNew)
        {
            using (TaskDbContext _db = new TaskDbContext())
            {                
                if (tskNew != null)
                {                    
                    _db.Tasks.Add(tskNew);
                    _db.SaveChanges();
                }
                else
                {
                    throw new ApplicationException("Unable to add the Task");
                }
            }
        }


        public bool deleteTask(int id)
        {

            using (TaskDbContext _db = new TaskDbContext())
            {
                var delteRecord = _db.Tasks.First(a => a.Task_ID == id);
                if (delteRecord != null)
                {
                    _db.Tasks.Remove(delteRecord);                    
                    _db.SaveChanges();                    
                }
                else
                {
                    throw new ApplicationException(string.Format("Task - {0}  Unable to remove the Task", delteRecord.Task_ID));
                }
                return true;
            }
        }

        public bool endTask(int id)
        {
            using (TaskDbContext _db = new TaskDbContext())
            {
                var endTaskRecord = _db.Tasks.First(a => a.Task_ID == id);
                if (endTaskRecord != null)
                {
                    endTaskRecord.isTaskEnd = true;                   
                    _db.SaveChanges();
                }
                else
                {
                    throw new ApplicationException(string.Format("Task - {0}  Unable to End the Task", id));
                }
            }
            return true;
        }

    }
}
