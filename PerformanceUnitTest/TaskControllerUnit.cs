﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using TaskMgr.Controllers;
using TaskMgr.Model;
using NBench;

namespace PerformanceUnitTest
{
    [TestClass]
    public class TaskControllerUnit
    {
        [PerfBenchmark(Description = "Performace Test for GET", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 200000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestAddTasks")]
        public void AddTasks_Test()
        {
            Task taskDetails = new Task();
            taskDetails.TaskName = "TestingUnit";
            taskDetails.Parent_ID = 5;
            taskDetails.Priority = 4;
            taskDetails.Start_Date = System.DateTime.Now;
            taskDetails.End_Date = System.DateTime.Now.AddDays(1);

            TaskController tc = new TaskController();
            tc.AddTask(taskDetails);

        }

        [PerfBenchmark(Description = "Performace Test for UPDATE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 200000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestUpdateTasks")]

        public void UpdateTasks_Test()
        {
            TaskController tc = new TaskController();
            Task taskDetails = new Task();
            taskDetails.TaskName = "TestingUnit14";
            taskDetails.Parent_ID = 5;
            taskDetails.Priority = 4;
            taskDetails.Start_Date = System.DateTime.Now;
            taskDetails.End_Date = System.DateTime.Now.AddDays(2);
            taskDetails.Task_ID = 3290;
            tc.UpdateTask(taskDetails);
        }

        [PerfBenchmark(Description = "Performace Test for GET All Task", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 200000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestGetAllTasks")]

        public void GetAllTasks_Test()
        {
            TaskController controller = new TaskController();
            controller.GetAllTasks();
        }

        [PerfBenchmark(Description = "Performace Test for GET TASK BY ID", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 200000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("TestGetTasksByTaskId")]
        public void GetTasksByTaskId_Test()
        {
            TaskController controller = new TaskController();
            controller.GetTasksByTaskId(3011);
        }
    }
}
